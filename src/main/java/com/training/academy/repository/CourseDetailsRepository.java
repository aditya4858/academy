package com.training.academy.repository;

import java.util.Optional;
import org.springframework.data.jpa.repository.JpaRepository;
import com.training.academy.entity.CourseDetails;


public interface CourseDetailsRepository extends JpaRepository<CourseDetails, String>{
	
	Optional<CourseDetails> findByCourseName(String courseName);
	
	Optional<CourseDetails> findByCourseCode(String courseName);
}