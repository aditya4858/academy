package com.training.academy.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.training.academy.entity.CourseDetails;
import com.training.academy.entity.TrainingCalender;

public interface TrainingCalenderRepository extends JpaRepository<TrainingCalender, Long>{

	TrainingCalender findByCourseDetails(CourseDetails courseCode);
}