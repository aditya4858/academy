package com.training.academy.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.training.academy.entity.User;

@Repository
public interface UserDetailsRepository extends JpaRepository<User, Long>{

	Optional<User> findByEmail(String email);
	Optional<User> findByMobileNumber(String mobileNumber);
}