package com.training.academy.repository;


import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.entity.User;

public interface EnrollmentDetailsRepository extends JpaRepository<EnrollmentDetails, Long>{

	List<EnrollmentDetails> findByEnrollmentIdAndUser(Long enrollmentId, User user);
	
	@Query("SELECT ed FROM EnrollmentDetails ed JOIN FETCH ed.user d WHERE ed.status = :status AND ed.user.userId = :userId")
//	@Query("SELECT ed FROM EnrollmentDetails ed JOIN FETCH ed.courseDetails WHERE ed.enrollmentId = :enrollmentId AND ed.user.userId = :userId")
    List<EnrollmentDetails> findByUserAndStatus(@Param("status") Status status, @Param("userId") Long userId);

	EnrollmentDetails findByUserAndTrainingCalender(User user, TrainingCalender trainingCalender);

	@Query("SELECT ed FROM EnrollmentDetails ed JOIN FETCH ed.trainingCalender d WHERE ed.trainingCalender.trainingId = :trainingId")
	List<EnrollmentDetails> findByTrainingId(@Param("trainingId") long trainingId);

	List<EnrollmentDetails> findByUser(User user);
}