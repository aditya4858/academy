package com.training.academy.service;

import java.util.List;
import java.util.Optional;

import com.training.academy.dto.UserDto;
import com.training.academy.entity.User;

public interface UserDetailsService {
	List<User> getAllUsers();
	UserDto createUser(UserDto user);
	User updateUser(Long id, User userDetails);
	Optional<User> getUserById(Long id);
	void deleteUser(Long id);
}