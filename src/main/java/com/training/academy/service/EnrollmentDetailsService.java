package com.training.academy.service;

import java.util.List;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.User;

public interface EnrollmentDetailsService {
	
	List<EnrollmentDetails> getEnrollmentDetails();

	List<EnrollmentDetails> findByEnrollmentIdAndUser(Long enrollmentId, User user);

	EnrollmentDetails addEnrollmentDetails(Long userId, Long trainingId);
}