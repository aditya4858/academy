package com.training.academy.service;

import java.util.Optional;

import com.training.academy.entity.TrainingCalender;
public interface TrainingCalendarService {
	
	TrainingCalender addTrainingCalendar(TrainingCalender trainingCalender);

	TrainingCalender updateTrainingCalendar(Long trainingId, TrainingCalender trainingCalender);

	Optional<TrainingCalender> getTrainingDetailsById(Long trainingId);

	void deleteTrainingCalender(long trainingId);
}