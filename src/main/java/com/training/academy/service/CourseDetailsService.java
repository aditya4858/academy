package com.training.academy.service;

import java.util.List;
import java.util.Optional;

import com.training.academy.entity.CourseDetails;

public interface CourseDetailsService {
	
	CourseDetails createCourse(CourseDetails courseDetails);
	
	CourseDetails updateCourse(String courseCode, CourseDetails courseDetails);

	List<CourseDetails> getCourseDetails();

	Optional<CourseDetails> getCourseDetailsById(String courseCode);

	void deleteCourseByCode(String courseCode);
}