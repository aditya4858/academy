package com.training.academy.entity;

import java.time.LocalDate;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "EnrollmentDetails")
public class EnrollmentDetails {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long enrollmentId;	
	
	@ManyToOne
    @JoinColumn(name = "userId")
    private User user;
	
	@ManyToOne
    @JoinColumn(name = "trainingId")
    private TrainingCalender trainingCalender;
	private LocalDate enrollementDate;
	
	@Enumerated(EnumType.STRING)
	private Status status;
}