package com.training.academy.entity;

public enum Status {
	SCHEDULED, INPROGRESS, CANCELLED, COMPLETED
}
