package com.training.academy.entity;

import java.time.LocalDate;
import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Data
@Entity
@Table(name = "TrainingCalender")
public class TrainingCalender {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long trainingId;

	@ManyToOne
	@JoinColumn(name = "courseCode")
	private CourseDetails courseDetails;

	private String trainerName;
	private String trainingDuration;

	private LocalDate startDate;
	private LocalDate endDate;

	@Enumerated(EnumType.STRING)
	private Status status;
}