package com.training.academy.scheduler;

//import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.training.academy.controller.UpdateCourseStatus;

@Component
public class CourseScheduler {
	    
	    private UpdateCourseStatus courseStatus;

	    //@Scheduled(cron = "0 */1 * * * ?")
	    public void updateCourseStatusScheduled() {
	    	courseStatus.updateCourseStatus();
	    }
}