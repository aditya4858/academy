package com.training.academy.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.academy.dto.UserDto;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.User;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.UserNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.service.UserDetailsService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/academy")
@RequiredArgsConstructor
public class UserDetailsController {
	
	private final UserDetailsService userDetailsService;
	private final EnrollmentDetailsRepository enRepository;

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/allUserDetails")
	public ResponseEntity<List<User>> getUserDetails() {
		logger.info("User details fetched successfully");
		return ResponseEntity.status(HttpStatus.OK).body(userDetailsService.getAllUsers());
	}
	
	@PostMapping("/addNewUser")
	public ResponseEntity<?> addNewUser(@RequestBody UserDto user) {
		try {
			UserDto createdUser = userDetailsService.createUser(user);
			return ResponseEntity.status(HttpStatus.OK).body(createdUser);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@GetMapping("/getUserDetails/{id}")
	public ResponseEntity<User> getUserById(@PathVariable Long id){
		User user = userDetailsService.getUserById(id).orElseThrow(() -> new UserNotFoundException("User not found"));
		return ResponseEntity.status(HttpStatus.OK).body(user);
	}
	
	@PutMapping("/updateUserDetails/{id}")
	public ResponseEntity<?> updateUser(@PathVariable Long id, @RequestBody User userDetails){
		try {
			User updatedUser = userDetailsService.updateUser(id, userDetails);
			return ResponseEntity.status(HttpStatus.OK).body(updatedUser);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
	
	@DeleteMapping("deleteUser/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable Long id){
		User user = userDetailsService.getUserById(id).orElseThrow(() -> new UserNotFoundException("User not found"));
		List<EnrollmentDetails> enrollmentDetails = enRepository.findByUser(user);
		if(enrollmentDetails.isEmpty()) {
			userDetailsService.deleteUser(user.getUserId());
			return ResponseEntity.status(HttpStatus.OK).body("User deleted successfully");
		}else {
			throw new CourseNotFoundException("User have enrollments.");
		}
	}
}