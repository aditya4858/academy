package com.training.academy.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.User;
import com.training.academy.exception.CustomerNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.UserDetailsRepository;
import com.training.academy.service.EnrollmentDetailsService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/academy")
@RequiredArgsConstructor
public class CourseCancelController {
	
	private final EnrollmentDetailsRepository enroDetailsRepository;
	
	private final UserDetailsRepository userDetailsRepository;
	
	private final EnrollmentDetailsService enrDetailsService;
	

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/cancelCourse/{enrollmentId}/user/{userId}")
	public ResponseEntity<List<EnrollmentDetails>> getCourseDetails(@PathVariable Long enrollmentId, @PathVariable Long userId) {
		logger.info("Enrollment details fetched successfully");
		User user = userDetailsRepository.findById(userId).orElseThrow(() -> new CustomerNotFoundException("User not found"));
		List<EnrollmentDetails> details = enrDetailsService.findByEnrollmentIdAndUser(enrollmentId,user);
		for(EnrollmentDetails enroll : details) {
			if(enroll.getStatus().equals(Status.COMPLETED)) {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body(details);
			}else if(enroll.getStatus().equals(Status.INPROGRESS)) {
				return ResponseEntity.status(HttpStatus.FORBIDDEN).body(details);
			}else if(enroll.getStatus().equals(Status.CANCELLED)) {
				return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(details);
			}else if(enroll.getStatus().equals(Status.SCHEDULED)) {
				enroll.setStatus(Status.CANCELLED);
			}
			enroDetailsRepository.save(enroll);
		}
		return ResponseEntity.status(HttpStatus.OK).body(details);
	}
}