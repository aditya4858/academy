package com.training.academy.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.academy.entity.CourseDetails;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.UserNotFoundException;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.service.CourseDetailsService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/academy")
public class CourseController {
    
    Logger logger = LoggerFactory.getLogger(getClass());
    
    private final CourseDetailsService courseDetailsService;
    
    private final TrainingCalenderRepository traRepository;
    
    @PostMapping("/addNewCourse")
	public ResponseEntity<?> addNewCourse(@RequestBody CourseDetails course) {
		try {
			CourseDetails createCourse = courseDetailsService.createCourse(course);
			return ResponseEntity.status(HttpStatus.OK).body(createCourse);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
    
    @PutMapping("/updateCourseDetails/{courseCode}")
	public ResponseEntity<?> updateUser(@PathVariable String courseCode, @RequestBody CourseDetails course){
    	try {
			CourseDetails createCourse = courseDetailsService.updateCourse(courseCode,course);
			return ResponseEntity.status(HttpStatus.OK).body(createCourse);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
    
    @GetMapping("/allCourseDetails")
	public ResponseEntity<List<CourseDetails>> getCourseDetails() {
		logger.info("Course details fetched successfully");
		return ResponseEntity.status(HttpStatus.OK).body(courseDetailsService.getCourseDetails());
	}
    
    @GetMapping("/getCourseDetailsById/{courseCode}")
	public ResponseEntity<CourseDetails> getCourseDetailsById(@PathVariable String courseCode){
		CourseDetails courseDetails = courseDetailsService.getCourseDetailsById(courseCode).orElseThrow(() -> new UserNotFoundException("Course not found"));
		return ResponseEntity.status(HttpStatus.OK).body(courseDetails);
	}
    
    @DeleteMapping("deleteCourseByCode/{courseCode}")
	public ResponseEntity<String> deleteCourseByCode(@PathVariable String courseCode){
    	CourseDetails courseDetails = courseDetailsService.getCourseDetailsById(courseCode).orElseThrow(() -> new UserNotFoundException("Course not found"));
		TrainingCalender trainingCalender = traRepository.findByCourseDetails(courseDetails);
		if(trainingCalender == null) {    	
	    	courseDetailsService.deleteCourseByCode(courseCode);
			return ResponseEntity.status(HttpStatus.OK).body("Course deleted successfully");
		}else {
			throw new CourseNotFoundException("Training Calender available for course.");
		}
	}
}