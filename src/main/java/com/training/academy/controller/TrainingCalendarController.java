package com.training.academy.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.UserNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.service.TrainingCalendarService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/academy")
public class TrainingCalendarController {
    
    Logger logger = LoggerFactory.getLogger(getClass());
    
    private final TrainingCalendarService trainCalendarService;
    
    private final TrainingCalenderRepository trRepository;
    
    private final EnrollmentDetailsRepository enRepository;
    
    @PostMapping("/addTrainingCalendar")
	public ResponseEntity<?> addTrainingCalendar(@RequestBody TrainingCalender trainCalender) {
		try {
			TrainingCalender createCalendar = trainCalendarService.addTrainingCalendar(trainCalender);
			return ResponseEntity.status(HttpStatus.OK).body(createCalendar);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
    @GetMapping("/allTrainingCalendarDetails")
	public ResponseEntity<List<TrainingCalender>> getTrainingCalendar() {
		logger.info("Training Calendar details fetched successfully");
		return ResponseEntity.status(HttpStatus.OK).body(trRepository.findAll());
	}
    
    @PutMapping("/updateTrainingCalendar/{trainingId}")
	public ResponseEntity<?> updateTrainingCalendar(@PathVariable Long trainingId, @RequestBody TrainingCalender trainingCalender){
		try {
			TrainingCalender trCalender = trainCalendarService.updateTrainingCalendar(trainingId, trainingCalender);
			return ResponseEntity.status(HttpStatus.OK).body(trCalender);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
    
    @GetMapping("/getTrainingCalenderDetails/{trainingId}")
	public ResponseEntity<TrainingCalender> getTrainingCalenderDetails(@PathVariable Long trainingId){
		TrainingCalender traiCalender = trainCalendarService.getTrainingDetailsById(trainingId).orElseThrow(() -> new UserNotFoundException("Training not found"));
		return ResponseEntity.status(HttpStatus.OK).body(traiCalender);
	}
    
    @DeleteMapping("deleteTrainingCalender/{trainingId}")
	public ResponseEntity<String> deleteTrainingCalender(@PathVariable Long trainingId){
    	TrainingCalender traiCalender = trainCalendarService.getTrainingDetailsById(trainingId).orElseThrow(() -> new UserNotFoundException("Training not found"));
    	List<EnrollmentDetails> enrollmentDetails = enRepository.findByTrainingId(trainingId);
    	if(enrollmentDetails.isEmpty()) {
    		trainCalendarService.deleteTrainingCalender(traiCalender.getTrainingId());
        	return ResponseEntity.status(HttpStatus.OK).body("Training Calender deleted successfully");
    	}else {
    		throw new CourseNotFoundException("Enrollment available for training.");
    	}
	}
}