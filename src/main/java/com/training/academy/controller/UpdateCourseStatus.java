package com.training.academy.controller;

import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UpdateCourseStatus {
	
	private final TrainingCalenderRepository trainingCalenderRepository;
	private final EnrollmentDetailsRepository enrollRepository;
	
	public void updateCourseStatus() {
		List<TrainingCalender> courses = trainingCalenderRepository.findAll();
		Date currentDate = new Date();
        for (TrainingCalender course : courses) {
        	Date courseStartDate = Date.from(course.getStartDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
        	Date courseEndDate = Date.from(course.getEndDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
        	List<EnrollmentDetails> enroll = enrollRepository.findByTrainingId(course.getTrainingId());
        	if(enroll !=null) {
        		for(EnrollmentDetails update : enroll) {
					if(currentDate.after(courseStartDate) && currentDate.before(courseEndDate)) {
						course.setStatus(Status.INPROGRESS);
						update.setStatus(Status.INPROGRESS);
					}else if(currentDate.after(courseEndDate)){
						course.setStatus(Status.COMPLETED);
						update.setStatus(Status.COMPLETED);
					}else {
						course.setStatus(Status.SCHEDULED);
						update.setStatus(Status.SCHEDULED);
					}
					trainingCalenderRepository.save(course);
					enrollRepository.save(update);
        		}
        	}
		}
	}
}