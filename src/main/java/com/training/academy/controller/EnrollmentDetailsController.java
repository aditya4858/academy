package com.training.academy.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.service.EnrollmentDetailsService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/academy")
public class EnrollmentDetailsController {
	
	private final EnrollmentDetailsService enrollDetailsService;

	Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/enrollmentDetails")
	public ResponseEntity<List<EnrollmentDetails>> getEnrollmentDetails() {
		logger.info("Enrollment details fetched successfully");
		return ResponseEntity.status(HttpStatus.OK).body(enrollDetailsService.getEnrollmentDetails());
	}
	
	@PostMapping("/addEnrollmentDetails")
	public ResponseEntity<?> addEnrollmentDetails(@RequestParam Long userId, @RequestParam Long trainingId) {
		try {
			EnrollmentDetails createEnrollment = enrollDetailsService.addEnrollmentDetails(userId, trainingId);
			return ResponseEntity.status(HttpStatus.OK).body(createEnrollment);
		}catch (RuntimeException e) {
			return ResponseEntity.badRequest().body(e.getMessage());
		}
	}
}