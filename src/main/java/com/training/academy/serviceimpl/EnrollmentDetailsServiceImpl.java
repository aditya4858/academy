package com.training.academy.serviceimpl;

import java.time.LocalDate;
import java.util.List;

import org.springframework.stereotype.Service;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.entity.User;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.CustomerNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.repository.UserDetailsRepository;
import com.training.academy.service.EnrollmentDetailsService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class EnrollmentDetailsServiceImpl implements EnrollmentDetailsService {
	
	private final EnrollmentDetailsRepository enroDetailsRepository;
	
	private final UserDetailsRepository userDetailsRepository;
	
	private final TrainingCalenderRepository traRepository;

	@Override
	public List<EnrollmentDetails> getEnrollmentDetails() {
		return enroDetailsRepository.findAll();
	}

	@Override
	public List<EnrollmentDetails> findByEnrollmentIdAndUser(Long enrollmentId, User user) {
		return enroDetailsRepository.findByEnrollmentIdAndUser(enrollmentId,user);
	}

	@Override
	public EnrollmentDetails addEnrollmentDetails(Long userId, Long trainingId) {
		User user = userDetailsRepository.findById(userId).orElseThrow(() -> new CustomerNotFoundException("User not found"));
		TrainingCalender trainingCalender = traRepository.findById(trainingId).orElseThrow(() -> new CustomerNotFoundException("Training not found"));
		if(trainingCalender.getStatus().equals(Status.INPROGRESS)) {
			throw new CourseNotFoundException("Cannot enroll, course is in progress");
		}
		List<EnrollmentDetails> enroll = enroDetailsRepository.findByUserAndStatus(Status.SCHEDULED, userId);		
		if(enroll.size() >= 3) {
			throw new CourseNotFoundException("Cannot enroll, already 3 courses scheduled");
		}
		
		EnrollmentDetails enroDetails = enroDetailsRepository.findByUserAndTrainingCalender(user, trainingCalender);
		if(enroDetails != null && (enroDetails.getStatus().equals(Status.SCHEDULED) || enroDetails.getStatus().equals(Status.INPROGRESS))) {
			throw new CourseNotFoundException("Already enrolled for this course");
		}
		
		EnrollmentDetails enrollmentDetails = new EnrollmentDetails();
		enrollmentDetails.setEnrollementDate(LocalDate.now());
		enrollmentDetails.setTrainingCalender(trainingCalender);
		enrollmentDetails.setStatus(Status.SCHEDULED);
		enrollmentDetails.setUser(user);
		
		return enroDetailsRepository.save(enrollmentDetails);
	}
}