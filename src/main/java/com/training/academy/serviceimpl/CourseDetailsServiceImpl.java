package com.training.academy.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;
import com.training.academy.entity.CourseDetails;
import com.training.academy.exception.CourseCodeAlreadyExistException;
import com.training.academy.exception.CourseNameAlreadyExistException;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.repository.CourseDetailsRepository;
import com.training.academy.service.CourseDetailsService;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class CourseDetailsServiceImpl implements CourseDetailsService {

	
	private final CourseDetailsRepository couDetailsRepository;
	
	public CourseDetails createCourse(CourseDetails courseDetails) {
		if(couDetailsRepository.findByCourseCode(courseDetails.getCourseCode()).isPresent()){
			throw new CourseCodeAlreadyExistException("Course Code already exists");
		}
		if(couDetailsRepository.findByCourseName(courseDetails.getCourseName()).isPresent()){
			throw new CourseNameAlreadyExistException("Course Name already exists");
		}
		CourseDetails course = new CourseDetails();
		course.setCourseCode(courseDetails.getCourseCode());
		course.setCourseName(courseDetails.getCourseName());
        return couDetailsRepository.save(course);
	}
	
	public CourseDetails updateCourse(String courseCode, CourseDetails courseDetails) {
		if(couDetailsRepository.findByCourseName(courseDetails.getCourseName()).isPresent()){
			throw new CourseNameAlreadyExistException("Course Name already exists");
		}
		CourseDetails course = couDetailsRepository.findByCourseCode(courseCode).orElseThrow(() -> new CourseNotFoundException("Course not found"));
		course.setCourseName(courseDetails.getCourseName());
        return couDetailsRepository.save(course);
	}

	@Override
	public List<CourseDetails> getCourseDetails() {
		return couDetailsRepository.findAll();
	}

	@Override
	public Optional<CourseDetails> getCourseDetailsById(String courseCode) {
		return couDetailsRepository.findById(courseCode);
	}

	@Override
	public void deleteCourseByCode(String courseCode) {
		couDetailsRepository.deleteById(courseCode);
	}
}