package com.training.academy.serviceimpl;

import java.time.ZoneId;
import java.util.Date;
import java.util.Optional;

import org.springframework.stereotype.Service;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.exception.CourseCodeAlreadyExistException;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.DateMismatchException;
import com.training.academy.repository.CourseDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.service.TrainingCalendarService;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Service
public class TrainingCalendarServiceImpl implements TrainingCalendarService {	
	
	private final TrainingCalenderRepository traRepository;

	private final CourseDetailsRepository couRepository;

	@Override
	public TrainingCalender addTrainingCalendar(TrainingCalender trainingCalender) {
		
		Date currentDate = new Date();
		Date courseStartDate = Date.from(trainingCalender.getStartDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
		TrainingCalender traCalender = traRepository.findByCourseDetails(trainingCalender.getCourseDetails());
		if(traCalender != null && (traCalender.getStartDate().equals(trainingCalender.getStartDate()))) {
			throw new CourseCodeAlreadyExistException("Course is already scheduled on same start date");
		}
		if(courseStartDate.before(currentDate)) {
			throw new DateMismatchException("Start date is before today");
		}
		couRepository.findById(trainingCalender.getCourseDetails().getCourseCode()).orElseThrow(() -> new CourseNotFoundException("Course not found"));
		TrainingCalender calender = new TrainingCalender();
		calender.setCourseDetails(trainingCalender.getCourseDetails());
		calender.setEndDate(trainingCalender.getStartDate().plusDays(Long.parseLong(trainingCalender.getTrainingDuration())).minusDays(1));
		calender.setStartDate(trainingCalender.getStartDate());
		calender.setStatus(trainingCalender.getStatus());
		calender.setTrainerName(trainingCalender.getTrainerName());
		calender.setTrainingDuration(trainingCalender.getTrainingDuration());
		return traRepository.save(calender);						
	}

	@Override
	public TrainingCalender updateTrainingCalendar(Long trainingId, TrainingCalender trainingCalender) {
		Date currentDate = new Date();
		Date courseStartDate = Date.from(trainingCalender.getStartDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
		TrainingCalender calender = traRepository.findById(trainingId).orElseThrow(() -> new RuntimeException("Training Calender not found"));
		if((calender.getStartDate().equals(trainingCalender.getStartDate()))) {
			throw new CourseCodeAlreadyExistException("Course is already scheduled on same start date");
		}
		if(courseStartDate.before(currentDate)) {
			throw new DateMismatchException("Start date is before today");
		}
		couRepository.findById(trainingCalender.getCourseDetails().getCourseCode()).orElseThrow(() -> new CourseNotFoundException("Course not found"));
		calender.setCourseDetails(trainingCalender.getCourseDetails());
		calender.setEndDate(trainingCalender.getStartDate().plusDays(Long.parseLong(trainingCalender.getTrainingDuration())).minusDays(1));
		calender.setStartDate(trainingCalender.getStartDate());
		calender.setStatus(trainingCalender.getStatus());
		calender.setTrainerName(trainingCalender.getTrainerName());
		calender.setTrainingDuration(trainingCalender.getTrainingDuration());
		return traRepository.save(calender);				
	}

	@Override
	public Optional<TrainingCalender> getTrainingDetailsById(Long trainingId) {
		return traRepository.findById(trainingId);
	}

	@Override
	public void deleteTrainingCalender(long trainingId) {
		traRepository.deleteById(trainingId);		
	}
}