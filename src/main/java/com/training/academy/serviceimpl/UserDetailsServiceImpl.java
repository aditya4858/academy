package com.training.academy.serviceimpl;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Service;

import com.training.academy.dto.UserDto;
import com.training.academy.entity.User;
import com.training.academy.exception.EmailAlreadyExistException;
import com.training.academy.exception.MobileNumberAlreadyExistException;
import com.training.academy.repository.UserDetailsRepository;
import com.training.academy.service.UserDetailsService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {
	
	private final UserDetailsRepository userDetailsRepository;
	
	public List<User> getAllUsers() {
		return userDetailsRepository.findAll();
	}

	public UserDto createUser(UserDto userDto) {
		if(userDetailsRepository.findByEmail(userDto.getEmail()).isPresent()){
			throw new EmailAlreadyExistException("Email already exists");
		}
		if(userDetailsRepository.findByMobileNumber(userDto.getMobileNumber()).isPresent()){
			throw new MobileNumberAlreadyExistException("Mobile number already exists");
		}
		User user = convertToEntity(userDto);
        User savedUser = userDetailsRepository.save(user);
        return convertToDto(savedUser);
	}
	
	private User convertToEntity(UserDto userDto) {
        User user = new User();
        user.setUserName(userDto.getUserName());
        user.setEmail(userDto.getEmail());
        user.setMobileNumber(userDto.getMobileNumber());
        return user;
    }
	 private UserDto convertToDto(User user) {
	        UserDto userDto = new UserDto();
	        userDto.setEmail(user.getEmail());
	        userDto.setMobileNumber(user.getMobileNumber());
	        userDto.setUserName(user.getUserName());
	        return userDto;
		}
	
	public Optional<User> getUserById(Long id) {
		return userDetailsRepository.findById(id);
	}
	
	public User updateUser(Long id, User userDetails) {
		User user = userDetailsRepository.findById(id).orElseThrow(() -> new RuntimeException("User not found"));
		if(!user.getEmail().equals(userDetails.getEmail()) &&
				userDetailsRepository.findByEmail(userDetails.getEmail()).isPresent()) {
			throw new EmailAlreadyExistException("Email already exists");			
		}
		if(!user.getMobileNumber().equals(userDetails.getMobileNumber()) &&
				userDetailsRepository.findByMobileNumber(userDetails.getMobileNumber()).isPresent()) {
			throw new MobileNumberAlreadyExistException("Mobile number already exists");	
		}
		user.setUserName(userDetails.getUserName());
		user.setEmail(userDetails.getEmail());
		user.setMobileNumber(userDetails.getMobileNumber());
		return userDetailsRepository.save(user);		
	}
	
	public void deleteUser(Long id) {
		userDetailsRepository.deleteById(id);
	}
}