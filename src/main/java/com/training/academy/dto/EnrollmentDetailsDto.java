package com.training.academy.dto;

import java.time.LocalDate;

import com.training.academy.entity.CourseDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.User;

public record EnrollmentDetailsDto(String enrollmentId, User userId, CourseDetails courseCode,
		String trainerName,String trainingDuration, LocalDate startDate,LocalDate endDate,
		Status status, LocalDate enrollementDate) {

}
