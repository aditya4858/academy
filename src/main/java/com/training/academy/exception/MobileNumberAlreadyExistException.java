package com.training.academy.exception;

public class MobileNumberAlreadyExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public MobileNumberAlreadyExistException(String message) {
		super(message);
	}
}