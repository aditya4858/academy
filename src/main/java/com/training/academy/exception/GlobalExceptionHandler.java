package com.training.academy.exception;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ValidationDto> handllerMethodAurgumentException(MethodArgumentNotValidException ex) {
		Map<String, String> errorMap = new HashMap<>();
		ex.getBindingResult().getFieldErrors()
				.forEach(error -> errorMap.put(error.getField(), error.getDefaultMessage()));
		ValidationDto validationMessage = new ValidationDto();
		validationMessage.setValidationErrors(errorMap);
		validationMessage.setErrorCode("FSA0004");
		validationMessage.setErrorMessage("please add valid input data");
		return new ResponseEntity<>(validationMessage, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(HttpMessageNotReadableException.class)
	public ResponseEntity<ErrorResponse> messageNotReadableHandler(HttpMessageNotReadableException ex) {
		ErrorResponse error = new ErrorResponse("FSA0003", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ErrorResponse> customerNotFoundExceptionHandler(CustomerNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("FSA0002", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<ErrorResponse> userNotFoundExceptionHandler(UserNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("FSA0001", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(EmailAlreadyExistException.class)
	public ResponseEntity<ErrorResponse> emailAlreadyExistException(EmailAlreadyExistException ex) {
		ErrorResponse error = new ErrorResponse("FSA0005", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MobileNumberAlreadyExistException.class)
	public ResponseEntity<ErrorResponse> mobileNumberAlreadyExistException(MobileNumberAlreadyExistException ex) {
		ErrorResponse error = new ErrorResponse("FSA0006", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CourseNameAlreadyExistException.class)
	public ResponseEntity<ErrorResponse> courseAlreadyExistException(CourseNameAlreadyExistException ex) {
		ErrorResponse error = new ErrorResponse("FSA0007", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CourseCodeAlreadyExistException.class)
	public ResponseEntity<ErrorResponse> courseCodeAlreadyExistException(CourseCodeAlreadyExistException ex) {
		ErrorResponse error = new ErrorResponse("FSA0008", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}

	

	@ExceptionHandler(DateMismatchException.class)
	public ResponseEntity<ErrorResponse> dateMismatchExceptionHandler(DateMismatchException ex) {
		ErrorResponse error = new ErrorResponse("EX0001", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(CourseNotFoundException.class)
	public ResponseEntity<ErrorResponse> courseNotFoundException(CourseNotFoundException ex) {
		ErrorResponse error = new ErrorResponse("FSA0009", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	public ResponseEntity<ErrorResponse> courseNotFoundExceptionHandler(DateMismatchException ex) {
		ErrorResponse error = new ErrorResponse("EX0002", ex.getMessage());
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}