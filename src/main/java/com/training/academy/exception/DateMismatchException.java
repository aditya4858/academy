package com.training.academy.exception;

public class DateMismatchException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public DateMismatchException(String message) {
		super(message);
	}

}
