package com.training.academy.exception;

public class CourseCodeAlreadyExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CourseCodeAlreadyExistException(String message) {
		super(message);
	}
}