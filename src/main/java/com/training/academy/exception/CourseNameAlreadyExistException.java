package com.training.academy.exception;

public class CourseNameAlreadyExistException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public CourseNameAlreadyExistException(String message) {
		super(message);
	}
}