package com.training.academy.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.training.academy.entity.CourseDetails;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.service.CourseDetailsService;

class CourseControllerTest {

    @Mock
    private CourseDetailsService courseDetailsService;

    @Mock
    private TrainingCalenderRepository trainingCalenderRepository;

    @Mock
    private Logger logger;

    @InjectMocks
    private CourseController courseController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testAddNewCourse_Success() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        when(courseDetailsService.createCourse(any())).thenReturn(courseDetails);

        // Act
        ResponseEntity<?> response = courseController.addNewCourse(courseDetails);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(courseDetails, response.getBody());
    }

    @Test
    void testAddNewCourse_Failure() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        RuntimeException exception = new RuntimeException("Error occurred");
        when(courseDetailsService.createCourse(any())).thenThrow(exception);

        // Act
        ResponseEntity<?> response = courseController.addNewCourse(courseDetails);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(exception.getMessage(), response.getBody());
    }

    @Test
    void testUpdateCourseDetails_Success() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        when(courseDetailsService.updateCourse(anyString(), any())).thenReturn(courseDetails);

        // Act
        ResponseEntity<?> response = courseController.updateUser("code", courseDetails);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(courseDetails, response.getBody());
    }

    @Test
    void testUpdateCourseDetails_Failure() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        RuntimeException exception = new RuntimeException("Error occurred");
        when(courseDetailsService.updateCourse(anyString(), any())).thenThrow(exception);

        // Act
        ResponseEntity<?> response = courseController.updateUser("code", courseDetails);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(exception.getMessage(), response.getBody());
    }

    @Test
    void testGetCourseDetails_Success() {
        // Arrange
        List<CourseDetails> courseList = new ArrayList<>();
        when(courseDetailsService.getCourseDetails()).thenReturn(courseList);

        // Act
        ResponseEntity<List<CourseDetails>> response = courseController.getCourseDetails();

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(courseList, response.getBody());
    }

    @Test
    void testGetCourseDetailsById_Success() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        when(courseDetailsService.getCourseDetailsById(anyString())).thenReturn(Optional.of(courseDetails));

        // Act
        ResponseEntity<CourseDetails> response = courseController.getCourseDetailsById("code");

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(courseDetails, response.getBody());
    }

    @Test
    void testGetCourseDetailsById_Failure() {
        // Arrange
    	CourseNotFoundException exception = new CourseNotFoundException("Course not found");
        when(courseDetailsService.getCourseDetailsById(anyString())).thenThrow(exception);

        // Act + Assert
        assertThrows(CourseNotFoundException.class, () -> courseController.getCourseDetailsById("code"));
    }

    @Test
    void testDeleteCourseByCode_Success() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        when(courseDetailsService.getCourseDetailsById(anyString())).thenReturn(Optional.of(courseDetails));
        when(trainingCalenderRepository.findByCourseDetails(any())).thenReturn(null);

        // Act
        ResponseEntity<String> response = courseController.deleteCourseByCode("code");

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals("Course deleted successfully", response.getBody());
    }

    @Test
    void testDeleteCourseByCode_Failure() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        when(courseDetailsService.getCourseDetailsById(anyString())).thenReturn(Optional.of(courseDetails));
        when(trainingCalenderRepository.findByCourseDetails(any())).thenReturn(new TrainingCalender());

        // Act + Assert
        assertThrows(CourseNotFoundException.class, () -> courseController.deleteCourseByCode("code"));
    }

}
