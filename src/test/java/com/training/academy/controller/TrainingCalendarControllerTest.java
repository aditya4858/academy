package com.training.academy.controller;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.service.TrainingCalendarService;

@WebMvcTest(TrainingCalendarController.class)
class TrainingCalendarControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private TrainingCalendarService trainCalendarService;

    @MockBean
    private TrainingCalenderRepository trRepository;

    @MockBean
    private EnrollmentDetailsRepository enRepository;

    private TrainingCalender trainingCalender;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        trainingCalender = new TrainingCalender();
        trainingCalender.setTrainingId(1L);
    }

    @Test
    void testAddTrainingCalendar_Success() throws Exception {
        when(trainCalendarService.addTrainingCalendar(any(TrainingCalender.class))).thenReturn(trainingCalender);
        mockMvc.perform(post("/academy/addTrainingCalendar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(trainingCalender)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.trainingId").value(trainingCalender.getTrainingId()));
    }

    @Test
    void testAddTrainingCalendar_RuntimeException() throws Exception {
        when(trainCalendarService.addTrainingCalendar(any(TrainingCalender.class))).thenThrow(new RuntimeException("Creation failed"));
        mockMvc.perform(post("/academy/addTrainingCalendar")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(trainingCalender)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Creation failed"));
    }

    @Test
    void testGetTrainingCalendar() throws Exception {
        when(trRepository.findAll()).thenReturn(Collections.singletonList(trainingCalender));

        mockMvc.perform(get("/academy/allTrainingCalendarDetails"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].trainingId").value(trainingCalender.getTrainingId()));
    }

    @Test
    void testUpdateTrainingCalendar_Success() throws Exception {
        when(trainCalendarService.updateTrainingCalendar(eq(1L), any(TrainingCalender.class))).thenReturn(trainingCalender);
        mockMvc.perform(put("/academy/updateTrainingCalendar/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(trainingCalender)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.trainingId").value(trainingCalender.getTrainingId()));
    }

    @Test
    void testUpdateTrainingCalendar_RuntimeException() throws Exception {
        when(trainCalendarService.updateTrainingCalendar(eq(1L), any(TrainingCalender.class))).thenThrow(new RuntimeException("Update failed"));
        mockMvc.perform(put("/academy/updateTrainingCalendar/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(trainingCalender)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Update failed"));
    }

    @Test
    void testGetTrainingCalenderDetails_Success() throws Exception {
        when(trainCalendarService.getTrainingDetailsById(1L)).thenReturn(Optional.of(trainingCalender));
        mockMvc.perform(get("/academy/getTrainingCalenderDetails/1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.trainingId").value(trainingCalender.getTrainingId()));
    }

    @Test
    void testGetTrainingCalenderDetails_NotFound() throws Exception {
        when(trainCalendarService.getTrainingDetailsById(1L)).thenReturn(Optional.empty());

        mockMvc.perform(get("/academy/getTrainingCalenderDetails/1"))
                .andExpect(status().isBadRequest());
    }

    @Test
    void testDeleteTrainingCalender_Success() throws Exception {
        when(trainCalendarService.getTrainingDetailsById(1L)).thenReturn(Optional.of(trainingCalender));
        when(enRepository.findByTrainingId(1L)).thenReturn(Collections.emptyList());
        mockMvc.perform(delete("/academy/deleteTrainingCalender/1"))
                .andExpect(status().isOk())
                .andExpect(content().string("Training Calender deleted successfully"));
    }

    @Test
    void testDeleteTrainingCalender_EnrollmentsExist() throws Exception {
        when(trainCalendarService.getTrainingDetailsById(1L)).thenReturn(Optional.of(trainingCalender));
        when(enRepository.findByTrainingId(1L)).thenReturn(Collections.singletonList(new EnrollmentDetails()));
        mockMvc.perform(delete("/academy/deleteTrainingCalender/1"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"errorCode\":\"FSA0009\",\"errorMessage\":\"Enrollment available for training.\"}"));
    }

    @Test
    void testDeleteTrainingCalender_NotFound() throws Exception {
        when(trainCalendarService.getTrainingDetailsById(1L)).thenReturn(Optional.empty());
        mockMvc.perform(delete("/academy/deleteTrainingCalender/1"))
                .andExpect(content().string("{\"errorCode\":\"FSA0001\",\"errorMessage\":\"Training not found\"}"));
    }
}