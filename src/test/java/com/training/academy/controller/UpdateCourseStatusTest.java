package com.training.academy.controller;

import static org.mockito.Mockito.*;
import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;

class UpdateCourseStatusTest {

    @InjectMocks
    private UpdateCourseStatus updateCourseStatus;

    @Mock
    private TrainingCalenderRepository trainingCalenderRepository;

    @Mock
    private EnrollmentDetailsRepository enrollRepository;

    private TrainingCalender trainingCalender;
    private EnrollmentDetails enrollmentDetails;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);

        trainingCalender = new TrainingCalender();
        trainingCalender.setTrainingId(1L);
        trainingCalender.setStartDate(LocalDate.now().minusDays(1));
        trainingCalender.setEndDate(LocalDate.now().plusDays(1));
        trainingCalender.setStatus(Status.SCHEDULED);

        enrollmentDetails = new EnrollmentDetails();
        enrollmentDetails.setTrainingCalender(trainingCalender);
        enrollmentDetails.setStatus(Status.SCHEDULED);
    }

    @Test
    void testUpdateCourseStatus_InProgress() {
        trainingCalender.setStartDate(LocalDate.now().minusDays(1));
        trainingCalender.setEndDate(LocalDate.now().plusDays(1));
        List<TrainingCalender> courses = Arrays.asList(trainingCalender);
        List<EnrollmentDetails> enrollments = Arrays.asList(enrollmentDetails);
        when(trainingCalenderRepository.findAll()).thenReturn(courses);
        when(enrollRepository.findByTrainingId(1L)).thenReturn(enrollments);
        updateCourseStatus.updateCourseStatus();
        verify(trainingCalenderRepository, times(1)).save(trainingCalender);
        verify(enrollRepository, times(1)).save(enrollmentDetails);
        assertEquals(Status.INPROGRESS, trainingCalender.getStatus());
        assertEquals(Status.INPROGRESS, enrollmentDetails.getStatus());
    }

    @Test
    void testUpdateCourseStatus_Completed() {
        trainingCalender.setStartDate(LocalDate.now().minusDays(2));
        trainingCalender.setEndDate(LocalDate.now().minusDays(1));
        List<TrainingCalender> courses = Arrays.asList(trainingCalender);
        List<EnrollmentDetails> enrollments = Arrays.asList(enrollmentDetails);
        when(trainingCalenderRepository.findAll()).thenReturn(courses);
        when(enrollRepository.findByTrainingId(1L)).thenReturn(enrollments);
        updateCourseStatus.updateCourseStatus();
        verify(trainingCalenderRepository, times(1)).save(trainingCalender);
        verify(enrollRepository, times(1)).save(enrollmentDetails);
        assertEquals(Status.COMPLETED, trainingCalender.getStatus());
        assertEquals(Status.COMPLETED, enrollmentDetails.getStatus());
    }

    @Test
    void testUpdateCourseStatus_Scheduled() {
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setEndDate(LocalDate.now().plusDays(2));
        List<TrainingCalender> courses = Arrays.asList(trainingCalender);
        List<EnrollmentDetails> enrollments = Arrays.asList(enrollmentDetails);
        when(trainingCalenderRepository.findAll()).thenReturn(courses);
        when(enrollRepository.findByTrainingId(1L)).thenReturn(enrollments);
        updateCourseStatus.updateCourseStatus();
        verify(trainingCalenderRepository, times(1)).save(trainingCalender);
        verify(enrollRepository, times(1)).save(enrollmentDetails);
        assertEquals(Status.SCHEDULED, trainingCalender.getStatus());
        assertEquals(Status.SCHEDULED, enrollmentDetails.getStatus());
    }

    @Test
    void testUpdateCourseStatus_NoEnrollments() {
        List<TrainingCalender> courses = Arrays.asList(trainingCalender);
        when(trainingCalenderRepository.findAll()).thenReturn(courses);
        when(enrollRepository.findByTrainingId(1L)).thenReturn(null);
        updateCourseStatus.updateCourseStatus();
        verify(enrollRepository, times(0)).save(any(EnrollmentDetails.class));
        assertEquals(Status.SCHEDULED, trainingCalender.getStatus());
    }

    @Test
    void testUpdateCourseStatus_EmptyCourses() {
        when(trainingCalenderRepository.findAll()).thenReturn(Collections.emptyList());
        updateCourseStatus.updateCourseStatus();
        verify(trainingCalenderRepository, times(0)).save(any(TrainingCalender.class));
        verify(enrollRepository, times(0)).save(any(EnrollmentDetails.class));
    }
}