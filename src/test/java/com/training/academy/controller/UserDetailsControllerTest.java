package com.training.academy.controller;

import org.junit.jupiter.api.Test;  
import org.junit.jupiter.api.extension.ExtendWith;  
import org.mockito.InjectMocks;  
import org.mockito.Mock;  
import org.mockito.junit.jupiter.MockitoExtension;  
import org.springframework.http.HttpStatus;  
import org.springframework.http.ResponseEntity;

import com.training.academy.dto.UserDto;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.User;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.UserNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.service.UserDetailsService;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;  
import java.util.Optional;  
 
import static org.junit.jupiter.api.Assertions.assertEquals;  
import static org.junit.jupiter.api.Assertions.assertNotNull;  
import static org.junit.jupiter.api.Assertions.assertThrows;   
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;  
 
@ExtendWith(MockitoExtension.class)  
public class UserDetailsControllerTest {  
 
   @InjectMocks  
   private UserDetailsController userController;  
 
   @Mock  
   private UserDetailsService userDetailsService;  
 
   @Mock  
   private EnrollmentDetailsRepository enRepository;  
 
   @Test  
   void testGetAllUserDetails() {  
       // Arrange  
       List<User> users = Arrays.asList(new User(), new User());  
       doReturn(users).when(userDetailsService).getAllUsers();  
 
       // Act  
       ResponseEntity<List<User>> response = userController.getUserDetails();  
 
       // Assert  
       assertEquals(HttpStatus.OK, response.getStatusCode());  
       assertNotNull(response.getBody());  
       assertEquals(users, response.getBody());  
   }  
 
   @Test  
   void testAddNewUser() {  
       // Arrange  
       UserDto userDto = new UserDto();  
       User createdUser = new User();  
       doReturn(createdUser).when(userDetailsService).createUser(userDto);  
 
       // Act  
       ResponseEntity<?> response = userController.addNewUser(userDto);  
 
       // Assert  
       assertEquals(HttpStatus.OK, response.getStatusCode());  
       assertNotNull(response.getBody());  
       assertEquals(createdUser, response.getBody());  
   }  
 
   @Test  
   void testAddNewUser_RuntimeException() {  
       // Arrange  
       UserDto userDto = new UserDto();  
       doThrow(new RuntimeException("Error creating user")).when(userDetailsService).createUser(userDto);  
 
       // Act and Assert  
       ResponseEntity<?> response = userController.addNewUser(userDto);  
       assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());  
       assertEquals("Error creating user", response.getBody());  
   }  
 
   @Test  
   void testGetUserById() {  
       // Arrange  
       Long userId = 1L;  
       User user = new User();  
       doReturn(Optional.of(user)).when(userDetailsService).getUserById(userId);  
 
       // Act  
       ResponseEntity<?> response = userController.getUserById(userId);  
 
       // Assert  
       assertEquals(HttpStatus.OK, response.getStatusCode());  
       assertNotNull(response.getBody());  
       assertEquals(user, response.getBody());  
   }  
 
   @Test  
   void testGetUserById_UserNotFoundException() {  
       // Arrange  
       Long userId = 1L;  
       doReturn(Optional.empty()).when(userDetailsService).getUserById(userId);  
 
       // Act and Assert  
       UserNotFoundException exception = assertThrows(UserNotFoundException.class, () -> userController.getUserById(userId));  
       assertEquals("User not found", exception.getMessage());  
   }  
 
   @Test  
   void testUpdateUser() {  
       // Arrange  
       Long userId = 1L;  
       User userDetails = new User();  
       User updatedUser = new User();  
       doReturn(updatedUser).when(userDetailsService).updateUser(userId, userDetails);  
 
       // Act  
       ResponseEntity<?> response = userController.updateUser(userId, userDetails);  
 
       // Assert  
       assertEquals(HttpStatus.OK, response.getStatusCode());  
       assertNotNull(response.getBody());  
       assertEquals(updatedUser, response.getBody());  
   }  
 
   @Test  
   void testUpdateUser_RuntimeException() {  
       // Arrange  
       Long userId = 1L;  
       User userDetails = new User();  
       doThrow(new RuntimeException("Error updating user")).when(userDetailsService).updateUser(userId, userDetails);  
 
       // Act and Assert  
       ResponseEntity<?> response = userController.updateUser(userId, userDetails);  
       assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());  
       assertEquals("Error updating user", response.getBody());  
   }  
 
   @Test  
   void testDeleteUser() {  
       // Arrange  
       Long userId = 1L;  
       User user = new User();  
       doReturn(Optional.of(user)).when(userDetailsService).getUserById(userId);  
       List<User> enrollmentDetails = Collections.emptyList();  
       doReturn(enrollmentDetails).when(enRepository).findByUser(user);  
 
       // Act  
       ResponseEntity<?> response = userController.deleteUser(userId);  
 
       // Assert  
       assertEquals(HttpStatus.OK, response.getStatusCode());  
       assertEquals("User deleted successfully", response.getBody());  
       verify(userDetailsService).deleteUser(userId);  
   }  
 
   @Test  
   void testDeleteUser_CourseNotFoundException() {  
       // Arrange  
       Long userId = 1L;  
       User user = new User();  
       doReturn(Optional.of(user)).when(userDetailsService).getUserById(userId);  
       List<EnrollmentDetails> enrollmentDetails = Arrays.asList(new EnrollmentDetails());  
       doReturn(enrollmentDetails).when(enRepository).findByUser(user);  
 
       // Act and Assert  
       CourseNotFoundException exception = assertThrows(CourseNotFoundException.class, () -> userController.deleteUser(userId));  
       assertEquals("User have enrollments.", exception.getMessage());  
   }  
}  
