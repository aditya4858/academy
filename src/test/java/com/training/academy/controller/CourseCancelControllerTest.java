package com.training.academy.controller;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.User;
import com.training.academy.exception.CustomerNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.UserDetailsRepository;
import com.training.academy.service.EnrollmentDetailsService;

class CourseCancelControllerTest {

    @Mock
    private EnrollmentDetailsRepository enrollmentDetailsRepository;

    @Mock
    private UserDetailsRepository userDetailsRepository;

    @Mock
    private EnrollmentDetailsService enrollmentDetailsService;

    @Mock
    private Logger logger;

    @InjectMocks
    private CourseCancelController courseCancelController;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetCourseDetails_UserNotFound() {
        // Arrange
        when(userDetailsRepository.findById(anyLong())).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(CustomerNotFoundException.class, () -> courseCancelController.getCourseDetails(1L, 1L));
    }

    @Test
    void testGetCourseDetails_StatusCompleted() {
        // Arrange
        User user = new User();
        when(userDetailsRepository.findById(anyLong())).thenReturn(Optional.of(user));

        EnrollmentDetails enrollmentDetails = new EnrollmentDetails();
        enrollmentDetails.setStatus(Status.COMPLETED);
        List<EnrollmentDetails> enrollmentDetailsList = new ArrayList<>();
        enrollmentDetailsList.add(enrollmentDetails);
        when(enrollmentDetailsService.findByEnrollmentIdAndUser(anyLong(), any())).thenReturn(enrollmentDetailsList);

        // Act
        ResponseEntity<List<EnrollmentDetails>> response = courseCancelController.getCourseDetails(1L, 1L);

        // Assert
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertEquals(enrollmentDetailsList, response.getBody());
    }

    @Test
    void testGetCourseDetails_StatusInProgress() {
        // Arrange
        User user = new User();
        when(userDetailsRepository.findById(anyLong())).thenReturn(Optional.of(user));

        EnrollmentDetails enrollmentDetails = new EnrollmentDetails();
        enrollmentDetails.setStatus(Status.INPROGRESS);
        List<EnrollmentDetails> enrollmentDetailsList = new ArrayList<>();
        enrollmentDetailsList.add(enrollmentDetails);
        when(enrollmentDetailsService.findByEnrollmentIdAndUser(anyLong(), any())).thenReturn(enrollmentDetailsList);

        // Act
        ResponseEntity<List<EnrollmentDetails>> response = courseCancelController.getCourseDetails(1L, 1L);

        // Assert
        assertEquals(HttpStatus.FORBIDDEN, response.getStatusCode());
        assertEquals(enrollmentDetailsList, response.getBody());
    }

    @Test
    void testGetCourseDetails_StatusCancelled() {
        // Arrange
        User user = new User();
        when(userDetailsRepository.findById(anyLong())).thenReturn(Optional.of(user));

        EnrollmentDetails enrollmentDetails = new EnrollmentDetails();
        enrollmentDetails.setStatus(Status.CANCELLED);
        List<EnrollmentDetails> enrollmentDetailsList = new ArrayList<>();
        enrollmentDetailsList.add(enrollmentDetails);
        when(enrollmentDetailsService.findByEnrollmentIdAndUser(anyLong(), any())).thenReturn(enrollmentDetailsList);

        // Act
        ResponseEntity<List<EnrollmentDetails>> response = courseCancelController.getCourseDetails(1L, 1L);

        // Assert
        assertEquals(HttpStatus.BAD_REQUEST, response.getStatusCode());
        assertEquals(enrollmentDetailsList, response.getBody());
    }

    @Test
    void testGetCourseDetails_StatusScheduled() {
        // Arrange
        User user = new User();
        when(userDetailsRepository.findById(anyLong())).thenReturn(Optional.of(user));

        EnrollmentDetails enrollmentDetails = new EnrollmentDetails();
        enrollmentDetails.setStatus(Status.SCHEDULED);
        List<EnrollmentDetails> enrollmentDetailsList = new ArrayList<>();
        enrollmentDetailsList.add(enrollmentDetails);
        when(enrollmentDetailsService.findByEnrollmentIdAndUser(anyLong(), any())).thenReturn(enrollmentDetailsList);

        // Act
        ResponseEntity<List<EnrollmentDetails>> response = courseCancelController.getCourseDetails(1L, 1L);

        // Assert
        assertEquals(HttpStatus.OK, response.getStatusCode());
        assertEquals(enrollmentDetailsList, response.getBody());
    }

    // Add more test cases to cover other scenarios...
}
