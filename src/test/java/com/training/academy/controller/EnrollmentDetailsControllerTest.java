package com.training.academy.controller;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import java.util.Collections;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.service.EnrollmentDetailsService;

@WebMvcTest(EnrollmentDetailsController.class)
class EnrollmentDetailsControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private EnrollmentDetailsService enrollDetailsService;

    private EnrollmentDetails enrollmentDetails;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        enrollmentDetails = new EnrollmentDetails();
        enrollmentDetails.setEnrollmentId(1L);
    }

    @Test
    void testGetEnrollmentDetails() throws Exception {
        when(enrollDetailsService.getEnrollmentDetails()).thenReturn(Collections.singletonList(enrollmentDetails));
        mockMvc.perform(get("/academy/enrollmentDetails"))
                .andExpect(status().isOk());
    }

    @Test
    void testAddEnrollmentDetails_Success() throws Exception {
        when(enrollDetailsService.addEnrollmentDetails(1L, 1L)).thenReturn(enrollmentDetails);
        mockMvc.perform(post("/academy/addEnrollmentDetails")
                .param("userId", "1")
                .param("trainingId", "1"))
                .andExpect(status().isOk());
    }

    @Test
    void testAddEnrollmentDetails_RuntimeException() throws Exception {
        when(enrollDetailsService.addEnrollmentDetails(1L, 1L)).thenThrow(new RuntimeException("Enrollment failed"));
        mockMvc.perform(post("/academy/addEnrollmentDetails")
                .param("userId", "1")
                .param("trainingId", "1"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("Enrollment failed"));
    }
}