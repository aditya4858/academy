package com.training.academy;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class AcademyApplicationTests {

	@Test
	void contextLoads() {
		Long actual = 1L;
		Long expected = 1L;
		assertEquals(actual, expected);
	}

}
