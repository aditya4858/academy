package com.training.academy.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.training.academy.entity.CourseDetails;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.exception.CourseCodeAlreadyExistException;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.exception.DateMismatchException;
import com.training.academy.repository.CourseDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.serviceimpl.TrainingCalendarServiceImpl;

class TrainingCalendarServiceImplTest {

    @Mock
    private TrainingCalenderRepository trainingCalenderRepository;

    @Mock
    private CourseDetailsRepository courseDetailsRepository;

    @InjectMocks
    private TrainingCalendarServiceImpl trainingCalendarService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testAddTrainingCalendar_Success() {
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setCourseDetails(new CourseDetails());
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setTrainingDuration("5");
        when(trainingCalenderRepository.findByCourseDetails(trainingCalender.getCourseDetails())).thenReturn(null);
        when(courseDetailsRepository.findById(any())).thenReturn(Optional.of(new CourseDetails()));
        when(trainingCalenderRepository.save(trainingCalender)).thenReturn(trainingCalender);
        TrainingCalender result = trainingCalendarService.addTrainingCalendar(trainingCalender);
        assertNull(result);
    }

    @Test
    void testAddTrainingCalendar_DuplicateCourseCode() {
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setCourseDetails(new CourseDetails());
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setTrainingDuration("5");
        TrainingCalender existingTrainingCalender = new TrainingCalender();
        existingTrainingCalender.setStartDate(trainingCalender.getStartDate());
        when(trainingCalenderRepository.findByCourseDetails(trainingCalender.getCourseDetails())).thenReturn(existingTrainingCalender);
        assertThrows(CourseCodeAlreadyExistException.class, () -> trainingCalendarService.addTrainingCalendar(trainingCalender));
    }

    @Test
    void testAddTrainingCalendar_CourseNotFound() {
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setCourseDetails(new CourseDetails());
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setTrainingDuration("5");
        when(trainingCalenderRepository.findByCourseDetails(trainingCalender.getCourseDetails())).thenReturn(null);
        when(courseDetailsRepository.findById(any())).thenReturn(Optional.empty());
        assertThrows(CourseNotFoundException.class, () -> trainingCalendarService.addTrainingCalendar(trainingCalender));
    }

    @Test
    void testUpdateTrainingCalendar_Success() {
    	CourseDetails courseDetails = new CourseDetails("C0001", "Java");
        Long trainingId = 1L;
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setTrainingDuration("5");
        trainingCalender.setCourseDetails(courseDetails);
        TrainingCalender existingTrainingCalender = new TrainingCalender();
        existingTrainingCalender.setStartDate(LocalDate.now().plusDays(2));
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(existingTrainingCalender));
        when(courseDetailsRepository.findById(any())).thenReturn(Optional.of(new CourseDetails()));
        when(trainingCalenderRepository.save(any())).thenReturn(trainingCalender);
        TrainingCalender result = trainingCalendarService.updateTrainingCalendar(trainingId, trainingCalender);
        assertNotNull(result);
    }

    @Test
    void testUpdateTrainingCalendar_CourseCodeAlreadyExist() {
        Long trainingId = 1L;
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setTrainingDuration("5");
        TrainingCalender existingTrainingCalender = new TrainingCalender();
        existingTrainingCalender.setStartDate(trainingCalender.getStartDate());
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(existingTrainingCalender));
        assertThrows(CourseCodeAlreadyExistException.class, () -> trainingCalendarService.updateTrainingCalendar(trainingId, trainingCalender));
    }

    @Test
    void testUpdateTrainingCalendar_StartDateBeforeCurrentDate() {
        Long trainingId = 1L;
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setCourseDetails(new CourseDetails());
        trainingCalender.setStartDate(LocalDate.now());
        trainingCalender.setTrainingDuration("5");        
        assertThrows(RuntimeException.class, () -> trainingCalendarService.updateTrainingCalendar(trainingId, trainingCalender));
    }
        
    @Test
    void testAddTrainingCalendar_StartDateBeforeCurrentDate() {
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setCourseDetails(new CourseDetails());
        trainingCalender.setStartDate(LocalDate.now().minusDays(1));
        trainingCalender.setTrainingDuration("5");
        assertThrows(DateMismatchException.class, () -> trainingCalendarService.addTrainingCalendar(trainingCalender));
    }

    @Test
    void testUpdateTrainingCalendar_CourseNotFound() {
        Long trainingId = 1L;
        TrainingCalender trainingCalender = new TrainingCalender();
        trainingCalender.setStartDate(LocalDate.now().plusDays(1));
        trainingCalender.setTrainingDuration("5");
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.empty());
        assertThrows(RuntimeException.class, () -> trainingCalendarService.updateTrainingCalendar(trainingId, trainingCalender));
    }

    @Test
    void testGetTrainingDetailsById_Success() {
        Long trainingId = 1L;
        TrainingCalender trainingCalender = new TrainingCalender();
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(trainingCalender));
        Optional<TrainingCalender> result = trainingCalendarService.getTrainingDetailsById(trainingId);
        assertTrue(result.isPresent());
        assertEquals(trainingCalender, result.get());
    }

    @Test
    void testGetTrainingDetailsById_NotFound() {
        Long trainingId = 1L;
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.empty());
        Optional<TrainingCalender> result = trainingCalendarService.getTrainingDetailsById(trainingId);
        assertTrue(result.isEmpty());
    }

    @Test
    void testDeleteTrainingCalender_Success() {
        long trainingId = 1L;
        assertDoesNotThrow(() -> trainingCalendarService.deleteTrainingCalender(trainingId));
        verify(trainingCalenderRepository, times(1)).deleteById(trainingId);
    }
}