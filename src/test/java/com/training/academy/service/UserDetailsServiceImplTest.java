package com.training.academy.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.training.academy.dto.UserDto;
import com.training.academy.entity.User;
import com.training.academy.exception.EmailAlreadyExistException;
import com.training.academy.exception.MobileNumberAlreadyExistException;
import com.training.academy.repository.UserDetailsRepository;
import com.training.academy.serviceimpl.UserDetailsServiceImpl;

class UserDetailsServiceImplTest {

    @Mock
    private UserDetailsRepository userDetailsRepository;

    @InjectMocks
    private UserDetailsServiceImpl userDetailsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllUsers() {
        // Arrange
        List<User> users = new ArrayList<>();
        when(userDetailsRepository.findAll()).thenReturn(users);

        // Act
        List<User> result = userDetailsService.getAllUsers();

        // Assert
        assertEquals(users, result);
    }

    @Test
    void testCreateUser_Success() {
        // Arrange
        UserDto userDto = new UserDto();
        userDto.setUserName("John Doe");
        userDto.setEmail("john.doe@example.com");
        userDto.setMobileNumber("1234567890");

        User user = new User();
        user.setUserName(userDto.getUserName());
        user.setEmail(userDto.getEmail());
        user.setMobileNumber(userDto.getMobileNumber());

        when(userDetailsRepository.findByEmail(userDto.getEmail())).thenReturn(Optional.empty());
        when(userDetailsRepository.findByMobileNumber(userDto.getMobileNumber())).thenReturn(Optional.empty());
        when(userDetailsRepository.save(any())).thenReturn(user);

        // Act
        UserDto result = userDetailsService.createUser(userDto);

        // Assert
        assertNotNull(result);
        assertEquals(userDto, result);
    }

    @Test
    void testCreateUser_EmailAlreadyExists() {
        // Arrange
        UserDto userDto = new UserDto();
        userDto.setEmail("john.doe@example.com");

        when(userDetailsRepository.findByEmail(userDto.getEmail())).thenReturn(Optional.of(new User()));

        // Act + Assert
        assertThrows(EmailAlreadyExistException.class, () -> userDetailsService.createUser(userDto));
    }

    @Test
    void testCreateUser_MobileNumberAlreadyExists() {
        // Arrange
        UserDto userDto = new UserDto();
        userDto.setMobileNumber("1234567890");

        when(userDetailsRepository.findByMobileNumber(userDto.getMobileNumber())).thenReturn(Optional.of(new User()));

        // Act + Assert
        assertThrows(MobileNumberAlreadyExistException.class, () -> userDetailsService.createUser(userDto));
    }

    @Test
    void testGetUserById() {
        // Arrange
        Long userId = 1L;
        User user = new User();
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(user));

        // Act
        Optional<User> result = userDetailsService.getUserById(userId);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(user, result.get());
    }

    @Test
    void testUpdateUser_Success() {
        // Arrange
        Long userId = 1L;
        User updatedUser = new User();
        updatedUser.setUserName("Jane Doe");
        updatedUser.setEmail("jane.doe@example.com");
        updatedUser.setMobileNumber("9876543210");

        User existingUser = new User();
        existingUser.setUserName("John Doe");
        existingUser.setEmail("john.doe@example.com");
        existingUser.setMobileNumber("1234567890");

        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userDetailsRepository.findByEmail(updatedUser.getEmail())).thenReturn(Optional.empty());
        when(userDetailsRepository.findByMobileNumber(updatedUser.getMobileNumber())).thenReturn(Optional.empty());
        when(userDetailsRepository.save(any())).thenReturn(existingUser);

        // Act
        User result = userDetailsService.updateUser(userId, updatedUser);

        // Assert
        assertNotNull(result);
        assertEquals(updatedUser.getUserName(), result.getUserName());
        assertEquals(updatedUser.getEmail(), result.getEmail());
        assertEquals(updatedUser.getMobileNumber(), result.getMobileNumber());
    }

    @Test
    void testUpdateUser_EmailAlreadyExists() {
        // Arrange
        Long userId = 1L;
        User updatedUser = new User();
        updatedUser.setEmail("jane.doe@example.com");

        User existingUser = new User();
        existingUser.setEmail("john.doe@example.com");

        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userDetailsRepository.findByEmail(updatedUser.getEmail())).thenReturn(Optional.of(new User()));

        // Act + Assert
        assertThrows(EmailAlreadyExistException.class, () -> userDetailsService.updateUser(userId, updatedUser));
    }

    @Test
    void testUpdateUser_MobileNumberAlreadyExists() {
        Long userId = 1L;
        User updatedUser = new User();
        updatedUser.setMobileNumber("9876543210");
        updatedUser.setEmail("jane.doe@example.com");

        User existingUser = new User();
        existingUser.setMobileNumber("9876543211");
        existingUser.setEmail("jane.doe@example.com");
        
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(existingUser));
        when(userDetailsRepository.findByMobileNumber(updatedUser.getMobileNumber())).thenReturn(Optional.of(new User()));
        
        assertThrows(MobileNumberAlreadyExistException.class, () -> userDetailsService.updateUser(userId, updatedUser));
    }

    @Test
    void testDeleteUser() {
        // Arrange
        Long userId = 1L;

        // Act
        assertDoesNotThrow(() -> userDetailsService.deleteUser(userId));

        // Assert
        verify(userDetailsRepository, times(1)).deleteById(userId);
    }
}
