package com.training.academy.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.training.academy.entity.CourseDetails;
import com.training.academy.exception.CourseCodeAlreadyExistException;
import com.training.academy.exception.CourseNameAlreadyExistException;
import com.training.academy.exception.CourseNotFoundException;
import com.training.academy.repository.CourseDetailsRepository;
import com.training.academy.serviceimpl.CourseDetailsServiceImpl;

class CourseDetailsServiceImplTest {

    @Mock
    private CourseDetailsRepository courseDetailsRepository;

    @InjectMocks
    private CourseDetailsServiceImpl courseDetailsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testCreateCourse_Success() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        courseDetails.setCourseCode("C001");
        courseDetails.setCourseName("Java");

        when(courseDetailsRepository.findByCourseCode("C001")).thenReturn(Optional.empty());
        when(courseDetailsRepository.findByCourseName("Java")).thenReturn(Optional.empty());
        when(courseDetailsRepository.save(courseDetails)).thenReturn(courseDetails);

        // Act
        CourseDetails result = courseDetailsService.createCourse(courseDetails);

        // Assert
        assertNotNull(result);
        assertEquals("C001", result.getCourseCode());
        assertEquals("Java", result.getCourseName());
    }

    @Test
    void testCreateCourse_DuplicateCourseCode() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        courseDetails.setCourseCode("C001");
        courseDetails.setCourseName("Java");

        when(courseDetailsRepository.findByCourseCode("C001")).thenReturn(Optional.of(courseDetails));

        // Act + Assert
        assertThrows(CourseCodeAlreadyExistException.class, () -> courseDetailsService.createCourse(courseDetails));
    }

    @Test
    void testCreateCourse_DuplicateCourseName() {
        // Arrange
        CourseDetails courseDetails = new CourseDetails();
        courseDetails.setCourseCode("C002");
        courseDetails.setCourseName("Java");

        when(courseDetailsRepository.findByCourseCode("C002")).thenReturn(Optional.empty());
        when(courseDetailsRepository.findByCourseName("Java")).thenReturn(Optional.of(courseDetails));

        // Act + Assert
        assertThrows(CourseNameAlreadyExistException.class, () -> courseDetailsService.createCourse(courseDetails));
    }

    @Test
    void testUpdateCourse_Success() {
        // Arrange
        String courseCode = "C001";
        CourseDetails courseDetails = new CourseDetails();
        courseDetails.setCourseCode(courseCode);
        courseDetails.setCourseName("Java");

        when(courseDetailsRepository.findByCourseCode(courseCode)).thenReturn(Optional.of(courseDetails));
        when(courseDetailsRepository.findByCourseName("Java")).thenReturn(Optional.empty());
        when(courseDetailsRepository.save(courseDetails)).thenReturn(courseDetails);

        // Act
        CourseDetails result = courseDetailsService.updateCourse(courseCode, courseDetails);

        // Assert
        assertNotNull(result);
        assertEquals("C001", result.getCourseCode());
        assertEquals("Java", result.getCourseName());
    }

    @Test
    void testUpdateCourse_CourseNotFound() {
        // Arrange
        String courseCode = "C001";
        CourseDetails courseDetails = new CourseDetails();

        when(courseDetailsRepository.findByCourseCode(courseCode)).thenReturn(Optional.empty());

        // Act + Assert
        assertThrows(CourseNotFoundException.class, () -> courseDetailsService.updateCourse(courseCode, courseDetails));
    }

    @Test
    void testUpdateCourse_DuplicateCourseName() {
        // Arrange
        String courseCode = "C001";
        CourseDetails courseDetails = new CourseDetails();
        courseDetails.setCourseCode(courseCode);
        courseDetails.setCourseName("Java");

        CourseDetails existingCourse = new CourseDetails();
        existingCourse.setCourseCode("C002");
        existingCourse.setCourseName("Python");

        when(courseDetailsRepository.findByCourseCode(courseCode)).thenReturn(Optional.of(courseDetails));
        when(courseDetailsRepository.findByCourseName("Java")).thenReturn(Optional.of(existingCourse));

        // Act + Assert
        assertThrows(CourseNameAlreadyExistException.class, () -> courseDetailsService.updateCourse(courseCode, courseDetails));
    }

    @Test
    void testGetCourseDetails_Success() {
        // Arrange
        List<CourseDetails> expectedCourses = new ArrayList<>();
        when(courseDetailsRepository.findAll()).thenReturn(expectedCourses);

        // Act
        List<CourseDetails> result = courseDetailsService.getCourseDetails();

        // Assert
        assertEquals(expectedCourses, result);
    }

    @Test
    void testGetCourseDetailsById_Success() {
        // Arrange
        String courseCode = "C001";
        CourseDetails courseDetails = new CourseDetails();
        courseDetails.setCourseCode(courseCode);

        when(courseDetailsRepository.findById(courseCode)).thenReturn(Optional.of(courseDetails));

        // Act
        Optional<CourseDetails> result = courseDetailsService.getCourseDetailsById(courseCode);

        // Assert
        assertTrue(result.isPresent());
        assertEquals(courseCode, result.get().getCourseCode());
    }

    @Test
    void testGetCourseDetailsById_CourseNotFound() {
        // Arrange
        String courseCode = "C001";
        when(courseDetailsRepository.findById(courseCode)).thenReturn(Optional.empty());

        // Act
        Optional<CourseDetails> result = courseDetailsService.getCourseDetailsById(courseCode);

        // Assert
        assertTrue(result.isEmpty());
    }

    @Test
    void testDeleteCourseByCode_Success() {
        // Arrange
        String courseCode = "C001";

        // Act
        assertDoesNotThrow(() -> courseDetailsService.deleteCourseByCode(courseCode));

        // Assert
        verify(courseDetailsRepository, times(1)).deleteById(courseCode);
    }
}
