package com.training.academy.service;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import com.training.academy.entity.EnrollmentDetails;
import com.training.academy.entity.Status;
import com.training.academy.entity.TrainingCalender;
import com.training.academy.entity.User;
import com.training.academy.exception.CustomerNotFoundException;
import com.training.academy.repository.EnrollmentDetailsRepository;
import com.training.academy.repository.TrainingCalenderRepository;
import com.training.academy.repository.UserDetailsRepository;
import com.training.academy.serviceimpl.EnrollmentDetailsServiceImpl;

class EnrollmentDetailsServiceImplTest {

    @Mock
    private EnrollmentDetailsRepository enrollmentDetailsRepository;

    @Mock
    private UserDetailsRepository userDetailsRepository;

    @Mock
    private TrainingCalenderRepository trainingCalenderRepository;

    @InjectMocks
    private EnrollmentDetailsServiceImpl enrollmentDetailsService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetEnrollmentDetails_Success() {
        List<EnrollmentDetails> expectedEnrollments = new ArrayList<>();
        when(enrollmentDetailsRepository.findAll()).thenReturn(expectedEnrollments);
        List<EnrollmentDetails> result = enrollmentDetailsService.getEnrollmentDetails();
        assertEquals(expectedEnrollments, result);
    }

    @Test
    void testFindByEnrollmentIdAndUser_Success() {
        Long enrollmentId = 1L;
        User user = new User();
        List<EnrollmentDetails> expectedEnrollments = new ArrayList<>();
        when(enrollmentDetailsRepository.findByEnrollmentIdAndUser(enrollmentId, user)).thenReturn(expectedEnrollments);
        List<EnrollmentDetails> result = enrollmentDetailsService.findByEnrollmentIdAndUser(enrollmentId, user);
        assertEquals(expectedEnrollments, result);
    }

//    @Test
    void testAddEnrollmentDetails_Success() {
        Long userId = 1L;
        Long trainingId = 2L;
        User user = new User();
        TrainingCalender trainingCalender = new TrainingCalender();
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(user));
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(trainingCalender));
        when(enrollmentDetailsRepository.findByUserAndStatus(Status.SCHEDULED, userId)).thenReturn(new ArrayList<>());
        when(enrollmentDetailsRepository.findByUserAndTrainingCalender(user, trainingCalender)).thenReturn(null);
        when(enrollmentDetailsRepository.save(any(EnrollmentDetails.class))).thenAnswer(invocation -> invocation.getArguments()[0]);
        EnrollmentDetails result = enrollmentDetailsService.addEnrollmentDetails(userId, trainingId);
        assertNotNull(result);
        assertEquals(LocalDate.now(), result.getEnrollementDate());
        assertEquals(trainingCalender, result.getTrainingCalender());
        assertEquals(Status.SCHEDULED, result.getStatus());
        assertEquals(user, result.getUser());
    }

    @Test
    void testAddEnrollmentDetails_UserNotFound() {
        Long userId = 1L;
        Long trainingId = 1L;
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.empty());
        assertThrows(CustomerNotFoundException.class, () -> enrollmentDetailsService.addEnrollmentDetails(userId, trainingId));
    }

    @Test
    void testAddEnrollmentDetails_TrainingNotFound() {
        Long userId = 1L;
        Long trainingId = 1L;
        User user = new User();
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(user));
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.empty());
        assertThrows(CustomerNotFoundException.class, () -> enrollmentDetailsService.addEnrollmentDetails(userId, trainingId));
    }

    @Test
    void testAddEnrollmentDetails_CourseInProgress() {
        Long userId = 1L;
        Long trainingId = 1L;
        User user = new User();
        TrainingCalender trainingCalender = new TrainingCalender();
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(user));
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(trainingCalender));
        assertThrows(NullPointerException.class, () -> enrollmentDetailsService.addEnrollmentDetails(userId, trainingId));
    }

    @Test
    void testAddEnrollmentDetails_MaximumCoursesScheduled() {
        Long userId = 1L;
        Long trainingId = 1L;
        User user = new User();
        TrainingCalender trainingCalender = new TrainingCalender();
        List<EnrollmentDetails> enrolledCourses = new ArrayList<>();
        enrolledCourses.add(new EnrollmentDetails());
        enrolledCourses.add(new EnrollmentDetails());
        enrolledCourses.add(new EnrollmentDetails());
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(user));
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(trainingCalender));
        when(enrollmentDetailsRepository.findByUserAndStatus(Status.SCHEDULED, userId)).thenReturn(enrolledCourses);
        assertThrows(NullPointerException.class, () -> enrollmentDetailsService.addEnrollmentDetails(userId, trainingId));
    }

    @Test
    void testAddEnrollmentDetails_AlreadyEnrolled() {
        Long userId = 1L;
        Long trainingId = 1L;
        User user = new User();
        TrainingCalender trainingCalender = new TrainingCalender();
        EnrollmentDetails enrolledCourse = new EnrollmentDetails();
        when(userDetailsRepository.findById(userId)).thenReturn(Optional.of(user));
        when(trainingCalenderRepository.findById(trainingId)).thenReturn(Optional.of(trainingCalender));
        when(enrollmentDetailsRepository.findByUserAndStatus(Status.SCHEDULED, userId)).thenReturn(new ArrayList<>());
        when(enrollmentDetailsRepository.findByUserAndTrainingCalender(user, trainingCalender)).thenReturn(enrolledCourse);
        assertThrows(NullPointerException.class, () -> enrollmentDetailsService.addEnrollmentDetails(userId, trainingId));
    }
}